#!/usr/bin/python3
import argparse
from collections import defaultdict
from distutils.dir_util import copy_tree, remove_tree
from glob import glob
import ipaddress
import os
from typing import Dict, List, Union
from tempfile import mkdtemp

from jinja2 import Environment, FileSystemLoader
from jinja2.exceptions import TemplateNotFound
import yaml

IPAddress = Union[ipaddress.IPv4Address, ipaddress.IPv6Address]


def ta_zonefile_to_trusted_keys(path: str) -> str:
    # BIND does not support TA in zonefile format, we have to preprocess that
    output = ["managed-keys {"]
    with open(path) as ta_zonefile:
        for line in ta_zonefile.readlines():
            tokens = line.split()

            try:
                dnskey_index = tokens.index("DNSKEY")
            except:
                continue

            tokens = tokens[dnskey_index+1:]

            output.append('"." initial-key %s %s %s "%s";' % (
                  tokens[0],
                  tokens[1],
                  tokens[2],
                  " ".join(tokens[3:])
              ))

    output.append("};")
    return "\n".join(output)

def ta_zonefile_to_pdns_lua(zonefile: str, lua_file: str) -> str:
    # PDNS' TA can only be configured using Lua config, we have to generate that
    rundir = os.path.dirname(zonefile)
    full_path = os.path.join(rundir, lua_file)
    with open(full_path, "w") as f:
        f.write("clearTA()\n")
        f.write("readTrustAnchorsFromFile('%s')\n" % zonefile)
    return full_path

def copy_enviroment(config: Dict[str, dict]) -> str:
    """Copy test enviroment to new temporary directory, returns its path."""
    path = config["globals"]["scenario"]
    tmpdir = mkdtemp(prefix="%s-%s-" % (config["globals"]["prefix"], os.path.basename(path)))
    for d in ["auth", "client", "resolver"]:
        copy_tree(os.path.join(path, d), os.path.join(tmpdir, d))
    os.mkdir(os.path.join(tmpdir, "services"))
    return tmpdir

def split_by_ip_address(config: Dict[str, dict]) -> Dict[IPAddress, dict]:
    ips = defaultdict(list)
    for path, cfg in config.items():
        if cfg.get("address") is None:
            continue
        for ip_string in cfg["address"]:
            ip = ipaddress.ip_address(ip_string)
            ips[ip].append((path, cfg))
    return ips

def prepare_timing(config: Dict[str, dict]):
    for cfg in config.values():
        if cfg.get("address") is None:
            continue
        cfg["previous"] = []
    config_by_ip = split_by_ip_address(config)
    for ip, configs in config_by_ip.items():
        configs = sorted(configs, key=lambda c: c[1].get("starttime", 0))
        for i in range(1, len(configs)):
            if configs[i-1][1].get("stoptime", float("inf")) > configs[i][1].get("starttime", 0):
                raise RuntimeError("Running time of %s and %s overlap on %s" %
                                   (configs[i-1][0], configs[i][0], ip))
            configs[i][1]["previous"].append(configs[i-1][1]["name"])

def load_configs(workdir: str, config: Dict[str, dict]):
    """Load directory structure and config.yaml files to a dictionary.
       Note that there will be NO config sanitazion since it's only used
       for filling in templates"""
    conffile = "config.yaml"
    netemid = 1

    for path, _, files in os.walk(workdir):
        relpath = os.path.relpath(path, workdir)
        if conffile in files:
            with open(os.path.join(path, conffile)) as f:
                config[relpath] = yaml.safe_load(f)
                config[relpath]["rundir"] = path
                config[relpath]["name"] = "%s-%s" % (config["globals"]["prefix"], config[relpath]["name"])
                config[relpath]["netemid"] = netemid
                netemid += 1

    prepare_timing(config)

# These two functions need a lot of error handling
# Also there is a lot of code repetition requiring non-trivial refactor :(

def render_config(path: str, config: Dict[str, dict]) -> None:
    conf = config[path]
    file_loader = FileSystemLoader(conf["rundir"])
    if conf.get("config") is None:
        return
    output_files = conf["config"] if isinstance(conf["config"], list) else [conf["config"]]

    for file_name in output_files:
        env = Environment(loader=file_loader)
        env.tests['ipv4'] = lambda ip: "." in ip
        env.tests['ipv6'] = lambda ip: ":" in ip
        env.filters['bind_ta'] = ta_zonefile_to_trusted_keys
        env.filters['pdns_ta'] = ta_zonefile_to_pdns_lua
        try:
            template = env.get_template(file_name+'.j2')
        except TemplateNotFound:
            file_loader = FileSystemLoader(config["globals"]["config_templates"])
            env = Environment(loader=file_loader)
            env.tests['ipv4'] = lambda ip: "." in ip
            env.tests['ipv6'] = lambda ip: ":" in ip
            env.filters['bind_ta'] = ta_zonefile_to_trusted_keys
            env.filters['pdns_ta'] = ta_zonefile_to_pdns_lua
            template = env.get_template(file_name+'.j2')
        with open(os.path.join(path, file_name), "w") as f:
            out = template.render(**conf, **config["globals"], c=config)
            f.write(out)


def render_service(path: str, config: Dict[str, dict]) -> None:
    conf = config[path]

    file_loader = FileSystemLoader(config["globals"]["service_templates"])
    env = Environment(loader=file_loader)
    env.tests['ipv4'] = lambda ip: "." in ip
    env.tests['ipv6'] = lambda ip: ":" in ip
    template = env.get_template("%s.service.j2" % conf["service"])
    service_name = conf["name"]
    with open(os.path.join("services", "%s.service" % service_name), "w") as f:
        f.write(template.render(**conf, **config["globals"], c=config))

    template = env.get_template("net.service.j2")
    with open(os.path.join("services", "%s-net.service" % conf["name"]), "w") as f:
        f.write(template.render(**conf, **config["globals"], c=config))

    if conf.get("starttime"):
        template = env.get_template("timer.service.j2")
        with open(os.path.join("services", "%s.timer" % conf["name"]), "w") as f:
            f.write(template.render(**conf, **config["globals"], c=config))

    return service_name

def render_namespace_service(config: Dict[str, dict]):
    file_loader = FileSystemLoader(config["globals"]["service_templates"])
    env = Environment(loader=file_loader)
    service_name = "namespace"
    template = env.get_template("%s.service.j2" % service_name)
    with open(os.path.join("services", "%s-%s.service" % (config["globals"]["prefix"], service_name)), "w") as f:
        f.write(template.render({
            "name": "%s-namespace" % config["globals"]["prefix"],
            "output": config["globals"]["pcap"]
        }, **config["globals"], c=config))

    return "%s-%s" % (config["globals"]["prefix"], service_name)


def generate_namespace(config: Dict[str, dict]):
    config["globals"]["NAMESPACE_SERVICE"] = render_namespace_service(config)

def generate_resolver(config: Dict[str, dict]):
    config["globals"]["RESOLVER_SERVICE"] = render_service("resolver", config)
    render_config("resolver", config)
    config["globals"]["RESOLVER_IPS"] = config["resolver"]["address"]

def generate_auths(config: Dict[str, dict]):
    for path in config:
        if path.startswith("auth"):
            if "config" in config[path]:
                render_config(path, config)
            render_service(path, config)

def generate_forwarders(config: Dict[str, dict]):
    config["globals"]["FORWARDERS"] = []
    for path in config:
        if path.startswith("forward"):
            if "config" in config[path]:
                render_config(path, config)
            config["globals"]["FORWARDERS"].append(render_service(path, config))

def generate_client(config: Dict[str, dict]):
    path = "client"
    if "config" in config[path]:
        render_config(path, config)
    return render_service(path, config)

def clean_up(prefix="maze-"):
    if not prefix:
        return
    for directory in glob("/tmp/%s*" % prefix):
        remove_tree(directory)



if __name__ == "__main__":
    current_dir = os.path.dirname(os.path.realpath(__file__))
    parser = argparse.ArgumentParser(description="Generates systemd services. Outputs a command to run them.", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("scenario", type=str, action="store", help="Path to scenario directory.")
    parser.add_argument("--prefix", type=str, action="store", default="maze", help="Prefix for systemd unit for easier clean-up.")
    parser.add_argument("--service-templates", type=str, action="store", default=os.path.join(current_dir, "systemd"), help="Path to directory with service templates.")
    parser.add_argument("--config-templates", type=str, action="store", default=os.path.join(current_dir, "configs"), help="Path to directory with config file templates.")
    parser.add_argument("--contrib", type=str, action="store", default=os.path.join(current_dir, "contrib"), help="Path to directory with external tools.")
    parser.add_argument("--pcap", type=str, action="store", default="/tmp/dump.pcap", help="Path to write a temporary pcap file.")
    parser.add_argument("--faketime", type=str, action="store", default="/usr/lib/faketime/libfaketime.so.1", help="Path to libfaketime shared object file.")

    # clean_up()
    config = {}
    config["globals"] = vars(parser.parse_args())
    config["globals"]["scenario"] = os.path.realpath(config["globals"]["scenario"])
    workdir = copy_enviroment(config)
    os.chdir(workdir)
    load_configs(workdir, config)
    generate_namespace(config)
    generate_resolver(config)
    generate_forwarders(config)
    generate_auths(config)
    print("sudo ./reload_services",
          workdir,
          config["globals"]["prefix"],
          generate_client(config),
          f'{config["globals"]["scenario"]}/pcaps/result_{config["resolver"]["service"]}.pcap')

