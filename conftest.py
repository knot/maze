import argparse
import pickle

import pytest

def interval(s):
    try:
        x, y = map(float, s.split(","))
        if not x < y:
            raise argparse.ArgumentTypeError("x,y must be a proper interval")
        return x, y
    except:
        raise argparse.ArgumentTypeError("x,y must be a proper interval")

def pytest_addoption(parser):
    parser.addoption("--under-test", required=True, type=str, action="store")
    parser.addoption("--reference", type=str, action="store")
    parser.addoption("--hist-intervals", type=interval, action="store", nargs="+")
    parser.addoption("--sw-intervals", type=interval, action="store", nargs="+")


def pytest_generate_tests(metafunc):

    if "under_test" in metafunc.fixturenames:
        path = metafunc.config.option.under_test
        if path is not None:
            with open(path, "rb") as f:
                metafunc.parametrize("under_test", [pickle.load(f)], ids=lambda _: f"under_test({path})")

    if "reference" in metafunc.fixturenames:
        path = metafunc.config.option.reference
        if path is not None:
            with open(path, "rb") as f:
                metafunc.parametrize("reference", [pickle.load(f)], ids=lambda _: f"reference({path})")

    if "hist_interval" in metafunc.fixturenames:
        intervals = metafunc.config.option.hist_intervals
        if intervals is None:
            intervals = []
        metafunc.parametrize("hist_interval", intervals, ids=lambda i: f"hist_interval({i})")

    if "sw_interval" in metafunc.fixturenames:
        intervals = metafunc.config.option.sw_intervals
        if intervals is None:
            intervals = []
        metafunc.parametrize("sw_interval", intervals, ids=lambda i: f"sw_interval({i})")

