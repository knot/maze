#!/usr/bin/python3
import argparse
from collections import defaultdict
import errno
from ipaddress import ip_address as ip
import math
import os.path
import pickle
import statistics
from typing import Dict, List

import dpkt

import process_generator


class PCAPAnalyzer:

    def __init__(self, pcap_path: str, scenario_dir: str):
        self.file = open(pcap_path, "rb")
        self.pcap_reader = dpkt.pcap.Reader(self.file)
        self.stats = defaultdict(lambda: defaultdict(int))
        self.scenario = scenario_dir
        self.config = {}
        self.config["globals"] = {"prefix": "maze"}
        process_generator.load_configs(self.scenario, self.config)
        self.auths_by_ip = defaultdict(list)
        self.parse_auths()
        self.resolver_ips = [ip(a) for a in self.config["resolver"]["address"]]
        self.client_ip = ip(self.config["client"]["address"][0])

    def __del__(self):
        self.file.close()

    def process_file(self):
        for ts, packet in self.pcap_reader:
            self.process_packet(ts, packet)
        q = self.stats["queries"]["from_client"]
        a = self.stats["answers"]["from_resolver"]
        if q != a:
            raise Warning("Resolver didn't answer all questions from client. %d queries and %d answers." % (q, a))

        self.sliding_window_rtt()

    def process_packet(self, ts, packet):
        for member_name in dir(self):
            # all methods starting with "_packet" are applied on each packet from PCAP
            if member_name.startswith("_packet"):
                # Unpack Linux cooked capture
                l2_packet = dpkt.sll.SLL(packet)
                getattr(self, member_name)(ts, l2_packet.data)

    def _packet_count_per_auth_ip(self, ts, ip_packet):
        ip_src = ip(ip_packet.src)
        ip_dst = ip(ip_packet.dst)
        if ip_src in self.resolver_ips and ip_dst != self.client_ip:
            self.stats["ip_dst"][ip_dst] += 1

    def _packet_count_per_server(self, ts, ip_packet):
        ip_src = ip(ip_packet.src)
        ip_dst = ip(ip_packet.dst)
        if ip_src in self.resolver_ips and ip_dst != self.client_ip:
            auth = self.identify_server(ts, ip_dst)
            if auth is None:
                auth = {}
                auth["name"] = "unknown("+str(ip_dst)+")"
                self.stats["queries"]["to_nonexistent_server"] += 1
            self.stats["server_dst"][auth["name"]] += 1
            self.stats["auth_config"][auth["name"]] = auth

    def _packet_dns_answer_rcode_count(self, ts, ip_packet):
        ip_src = ip(ip_packet.src)
        ip_dst = ip(ip_packet.dst)
        if ip_src in self.resolver_ips and ip_dst == self.client_ip:
            dns_packet = dpkt.dns.DNS(ip_packet.data.data)
            self.stats["rcode"][dns_packet.rcode] += 1

    def _packet_dns_query_count(self, ts, ip_packet):
        ip_src = ip(ip_packet.src)
        ip_dst = ip(ip_packet.dst)
        if ip_src in self.resolver_ips:
            if ip_dst != self.client_ip:
                self.stats["queries"]["from_resolver"] += 1
            else:
                self.stats["answers"]["from_resolver"] += 1
        if ip_dst in self.resolver_ips:
            if ip_src == self.client_ip:
                self.stats["queries"]["from_client"] += 1
            else:
                self.stats["answers"]["from_auths"] += 1


    def _packet_get_rtts(self, ts, ip_packet):
        ip_src = ip(ip_packet.src)
        ip_dst = ip(ip_packet.dst)
        if ip_src == self.client_ip:
            dns_packet = dpkt.dns.DNS(ip_packet.data.data)
            self.stats["rtts"][getattr(dns_packet, "id")] = -ts
        if ip_src in self.resolver_ips and ip_dst == self.client_ip:
            dns_packet = dpkt.dns.DNS(ip_packet.data.data)
            self.stats["rtts"][getattr(dns_packet, "id")] += ts

    def _packet_count_queries_per_qname(self, ts, ip_packet):
        ip_src = ip(ip_packet.src)
        ip_dst = ip(ip_packet.dst)
        if ip_src in self.resolver_ips and ip_dst != self.client_ip:
            if not isinstance(ip_packet.data.data, dpkt.icmp.ICMP.Unreach):
                dns_packet = dpkt.dns.DNS(ip_packet.data.data)
                self.stats["queries_by_qname"][dns_packet.qd[0].name.lower()] += 1


    def sliding_window_rtt(self, window = 100):
        # Window width of 100 yields meaningful results for now.
        # Testing was done using versions: kresd 5.0.1, unbound 1.9.6,
        # pdns 4.2.1, bind 9.14.10
        window_means = self.stats["sliding_window"][window] = []
        rtts = list(self.stats["rtts"].values())
        for i in range(window, len(rtts)):
            window_means.append(statistics.mean(rtts[i-window:i]))

    def parse_auths(self):
        for path, config in self.config.items():
            if path.startswith("auth"):
                for address in [ip(addr) for addr in config["address"]]:
                    self.auths_by_ip[address].append(config)

    def identify_server(self, ts, ip_addr):
        candidates = self.auths_by_ip[ip_addr]

        for config in candidates:
            if config.get("startime") is None and config.get("stoptime") is None:
                return config
            if config["starttime"] <= ts <= config["stoptime"]:
                return config

        return None

def default_to_regular(d):
        if isinstance(d, defaultdict):
            d = {k: default_to_regular(v) for k, v in d.items()}
        return d

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Computes various stats from PCAP and scenario. Outputs a pickled `dict` with computed stats.")
    parser.add_argument("--pcap", required=True, type=str, action="store", help="Path to pcap to analyze")
    parser.add_argument("--scenario", required=True, type=str, action="store", help="Path to scenario to analyze")
    parser.add_argument("--reference", action="store_true", help="Save output to 'references' folder of the scenario.")

    args = parser.parse_args()

    p = PCAPAnalyzer(args.pcap, args.scenario)
    p.process_file()

    if args.reference:
        dir_path = os.path.join(args.scenario, "references")
        try:
            os.makedirs(dir_path)
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise
    else:
        dir_path = args.scenario

    filename = os.path.basename(os.path.splitext(args.pcap)[0]) + "_stats.pickle"
    output = os.path.join(dir_path, filename)

    with open(output, "wb") as f:
        pickle.dump(default_to_regular(p.stats), f)

    print(output)





