#!/usr/bin/python3

import argparse
from collections import OrderedDict
import math
import os
import re
import sys
import pickle

import matplotlib.ticker as mtick
import matplotlib.pyplot as plt
import numpy as np
from typing import Dict, List

# pyplot global settings
plt.rcParams["font.family"] = "monospace"
plt.rcParams["font.size"] = 16
plt.rcParams["figure.figsize"] = (2*9.2, 2*4)
plt.rcParams["figure.dpi"] = 300
plt.tight_layout()
TITLE = True  # plot diagram titles
SAVEFIG_ARGS = {"bbox_inches": 'tight', "pad_inches": 0.1}
NAME_DICT = {"kresd": "kresd",
             "recursor": "pdns",
             "named": "bind",
             "unbound": "unbound"}
COLOR_DICT = {"kresd": "blue",
             "recursor": "orange",
             "named": "black",
             "unbound": "green",
             "previous": "grey"}

def autolabel(rects, labels, ax, xpos='center', format_str='{0:.0%}'):
    """
    Attach a text label above each bar in *rects*, displaying its height.

    *xpos* indicates which side to place the text w.r.t. the center of
    the bar. It can be one of the following {'center', 'right', 'left'}.
    """

    xpos = xpos.lower()  # normalize the case of the parameter
    ha = {'center': 'center', 'right': 'left', 'left': 'right'}
    offset = {'center': 0.5, 'right': 0.57, 'left': 0.43}  # x_txt = x + w*off

    for rect, label in zip(rects, labels):
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width()*offset[xpos], 1.01*height,
                format_str.format(label), ha=ha[xpos], va='bottom')

def split_netem_string(netem):
    l = netem.split()
    if not l:
        return ""
    for w in ["delay", "loss", "random"]:
        if w in l:
            l.remove(w)

    if len(l) == 3:
        l[0] = l[0][:l[0].index(".")+2]+" ms"
        l[1] = l[1][:l[1].index(".")+2]+" ms"
        l[2] = l[2][:l[2].index(".")+2]+" %"
        return l[0]+" ±"+l[1]+"\n"+l[2] if l[2] != "0.0 %" else l[0]+" ±"+l[1]

    return "\n".join(l)


def plot_log_percentile_histogram(
            data: Dict[str, List[float]],
            scenario: str,
            colors=None,
            folder=None
        ) -> None:
    """
    For graph explanation, see
    https://blog.powerdns.com/2017/11/02/dns-performance-metrics-the-logarithmic-percentile-histogram/
    """
    fig, ax = plt.subplots()


    # Distribute sample points along logarithmic X axis
    percentiles = np.logspace(-3, 2, num=10000)

    def tick_formatter(x, _):
        packets = int(len(data[next(data.keys().__iter__())])/100*x)
        noun = "packet" + ("" if packets == 1 else "s")
        return "%s\n%d %s" % (x, packets, noun)

    ax.set_xscale('log')
    ax.xaxis.set_major_formatter(mtick.FuncFormatter(tick_formatter))
    ax.set_yscale('log')
    ax.yaxis.set_major_formatter(mtick.FormatStrFormatter('%s'))

    ax.grid(True, which='major')
    ax.grid(True, which='minor', linestyle='dotted', color='#DDDDDD')
    ax.set_xlabel('Slowest percentile')
    ax.set_ylabel('Response time [ms]')

    if TITLE:
        ax.set_title('Resolver Response Time' + " - " + scenario)

    # plot data
    for name, rtts in data.items():
        # convert to ms and sort
        values = sorted([1000 * x for x in rtts], reverse=True)

        ax.plot(percentiles, [values[math.ceil(pctl * len(values) / 100) - 1] for pctl in percentiles],
                lw=4, label=name, color=colors[name])

    ax.legend()
    fig.savefig(os.path.join(folder, "comparison_histogram.svg"), **SAVEFIG_ARGS)

def plot_stat(resolver_name: str, resolver_data: dict, stat: str, scenario: str, folder, reverse=False):
    _, ax = plt.subplots()
    d = resolver_data[stat]
    d = OrderedDict(sorted(d.items(), key=lambda t: t[1], reverse=reverse))
    ax.bar(range(len(d)), list(d.values()), align='center')
    plt.xticks(range(len(d)), list(d.keys()), rotation='vertical')
    if TITLE:
        ax.set_title("%s - %s - %s" % scenario, stat, resolver_name)
    plt.savefig(os.path.join(folder, f"{resolver_name}_{stat}.svg"), bbox_inches='tight', pad_inches=0.1)

def plot_packet_distribution(resolver_name, resolver_data, scenario, folder):
    _, ax = plt.subplots()
    d = resolver_data["server_dst"]
    d = OrderedDict(sorted(d.items(), key=lambda t: t[0], reverse=False))
    rects = ax.bar(range(len(d)), list(d.values()), align='center')
    config = resolver_data["auth_config"]
    ticks = [split_netem_string(config[key].get("netem", "")) for key in d.keys()]  # TODO Fix netem string parsing
    plt.xticks(range(len(d)), ticks, rotation='vertical', fontsize=15)
    ax.set_title("%s - %s - %s" % (scenario, "Packets to auths distribution", name))
    ax.set_ylabel('number of queries')
    q = resolver_data["queries"]["from_resolver"]
    percentages = [k/q for k in d.values()]
    autolabel(rects, percentages, ax)

    plt.savefig(os.path.join(folder, f"{resolver_name}_packet_distribution.svg"), **SAVEFIG_ARGS)

def plot_resolver_queries(full_data, scenario, folder):
    data = {}
    percentages = {}
    for name, stats in full_data.items():
        q = stats["queries"]["from_client"]
        data[name] = stats["queries"]["from_resolver"]
        percentages[name] = data[name]/q
    _, ax = plt.subplots()
    rects = ax.bar(range(len(data)), list(data.values()), align='center')
    plt.xticks(range(len(data)), list(data.keys()), rotation='vertical')
    if TITLE:
        ax.set_title("%s - %s" % (scenario, "queries from resolver"))
    plt.axhline(y=q,linewidth=1, color='k', ls="--")
    plt.text(3.45, q+75, "100%")
    autolabel(rects, percentages.values(), ax)
    plt.ylabel("number of packets")
    plt.savefig(os.path.join(folder, "comparison_number_of_packets.svg"), **SAVEFIG_ARGS)

def plot_lost_packets(full_data, scenario, folder):
    data = {}
    for name, resolver_data in full_data.items():
        data[name] = resolver_data["queries"].get("to_nonexistent_server", 0)
    _, ax = plt.subplots()
    rects = ax.bar(range(len(data)), list(data.values()), align='center')
    plt.xticks(range(len(data)), list(data.keys()), rotation='vertical')
    plt.ylabel("number of packets")

    if TITLE:
        ax.set_title("%s - %s" % (scenario, "queries to dead address"))

    autolabel(rects, data.values(), ax, format_str="{}")

    plt.savefig(os.path.join(folder, "comparison_packets_to_nonexistent.svg"), **SAVEFIG_ARGS)

def plot_servfails(full_data, scenario, folder):
    data = {}
    for name, resolver_data in full_data.items():
        q = resolver_data["queries"]["from_client"]
        data[name] = resolver_data["rcode"].get(2, 0)

    _, ax = plt.subplots()
    ax.bar(range(len(data)), list(data.values()), align='center')
    plt.xticks(range(len(data)), list(data.keys()), rotation='vertical')

    if TITLE:
        ax.set_title("%s - %s" % (os.path.basename(scenario), f"SERVFAILs out of {q}"))

    plt.ylabel("number of SERVFAILs returned")

    plt.savefig(os.path.join(folder, "comparison_servfails.svg"), **SAVEFIG_ARGS)


def plot_sliding_windows(full_data, scenario, folder, window=100):
    # TODO Add percentages to ticks
    _, ax = plt.subplots()

    if TITLE:
        ax.set_title(f"{scenario}, sliding window = {window} queries")

    ymax = 0

    for name in full_data:
        windows = [1000*v for v in full_data[name]["sliding_window"][100]]
        m = max(windows)
        if ymax < m:
            ymax = m
        ax.plot(windows, label=name, lw=4, color=full_data[name]["color"])

    plt.ylim(0, ymax)
    ax.set_xlabel('Client query number')
    ax.set_ylabel('Average response time over the window [ms]')

    ax.legend()

    plt.savefig(os.path.join(folder, "comparison_sliding_windows.svg"), **SAVEFIG_ARGS)



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Generate graphs for scnenario results and their comparasions.")
    parser.add_argument("--scenario", required=True, help="Path to scenario folder.")
    parser.add_argument("--resolver", required=True, action="append", help="Paths to pickled data")
    args = parser.parse_args()

    scenario_path = args.scenario
    graphs_path = os.path.join(scenario_path, "graphs")
    os.makedirs(graphs_path, exist_ok=True)
    scenario_name = os.path.basename(os.path.normpath(scenario_path))

    full_data = {}
    for filename in args.resolver:
        basename = os.path.basename(filename).split("_", 2)[1].rsplit(".", 2)[0]
        name = NAME_DICT.get(basename, basename)

        with open(filename, "rb") as f:
            full_data[name] = pickle.load(f)
        full_data[name]["color"] = COLOR_DICT.get(basename, None)

    rtts = {name: list(stats["rtts"].values()) for name, stats in full_data.items()}
    colors = {name: stats["color"] for name, stats in full_data.items()}

    # Comparative plots
    plot_log_percentile_histogram(rtts, f"{scenario_name}", colors, graphs_path)
    plot_resolver_queries(full_data, scenario_name, graphs_path)
    plot_lost_packets(full_data, scenario_name, graphs_path)
    plot_servfails(full_data, scenario_name, graphs_path)
    plot_sliding_windows(full_data, scenario_name, graphs_path)

    # Per-resolver plots
    for name, resolver_data in full_data.items():
        plot_packet_distribution(name, resolver_data, scenario_name, graphs_path)





