#!/usr/bin/python3

import argparse
import json
from typing import Iterable, List, Optional
import os
import urllib.request
import statistics
import string
import time
import yaml

ROOT_SERVERS = string.ascii_lowercase[:13]
API_URL = "https://atlas.ripe.net/api/v2/measurements/"

def read_json(root_server: str,
              start_time: int,
              stop_time: int,
              ip_version: int,
              transport: str,
              probe: int,
             ):
    with open("ripe_measurements.yaml", "r") as f:
        measurements = yaml.safe_load(f)

    url = API_URL
    url += str(measurements[root_server][ip_version][transport])
    url += "/results/?format=json&"
    url += "probe_ids=%d&" % probe
    if start_time:
        url += "start=%d&" % start_time
    if stop_time:
        url += "stop=%d&" % stop_time

    print(url)

    with urllib.request.urlopen(url) as u:
        return json.loads(u.read().decode())

def collect_rtts(measurements: Iterable[dict]):
    for m in measurements:
        try:
            yield m["result"]["rt"]
        except KeyError:
            yield None

def calculate_netem(measurements: Iterable[dict], no_jitter: bool, no_loss: bool):
    rtts = list(collect_rtts(measurements))
    print(rtts)
    if no_loss:
        loss = 0
    else:
        loss = rtts.count(None)/len(rtts)*100
    rtts = [r for r in rtts if r is not None]
    if no_jitter:
        median = statistics.median(rtts)
        return "delay %.5fms loss random %.5f%%" % (median, loss)
    else:
        mean = statistics.mean(rtts)
        jitter = statistics.stdev(rtts, mean)
        return "delay %.5fms %.5fms loss random %.5f%%" % (mean, jitter, loss)

def generate_root_servers(start_time: int,
                          stop_time: int,
                          ip_versions: List[int],
                          probe: int,
                          no_jitter: bool,
                          no_loss: bool):

    root_hints = []
    root_configs = {}
    for server in ROOT_SERVERS:
        name = "%s.root-server.net" % server
        root_hints.append(".\t3600000\tIN\tNS\t%s.\n" % name)
        for ip_version in ip_versions:
            measurements = read_json(server, start_time, stop_time, ip_version, "udp", probe)
            config = {}
            config["name"] = "%s-ipv%d" % (name.replace(".","-"), ip_version)
            config["domain"] = "."
            config["service"] = "knot"
            config["config"] = "knotd.conf"
            config["netem"] = calculate_netem(measurements, no_jitter, no_loss)
            config["address"] = [measurements[0]["dst_addr"]]
            config["zonefile"] = "../root.zone"
            root_hints.append("%s.\t3600000\tIN\t\t%s\t%s\n" % (name, "AAAA" if ip_version == 6 else "A", config["address"][0]))
            root_configs[server+"-ipv%d" % ip_version] = config

    return root_configs, root_hints

def generate_scenario_stub(start_time: int,
                           stop_time: int,
                           ip_versions: List[int],
                           probe: int,
                           no_jitter: bool,
                           no_loss: bool):
    root_configs, root_hints = generate_root_servers(start_time, stop_time, ip_versions, probe, no_jitter, no_loss)
    dir_name = os.path.join("scenarios", "root_probe_%d" % probe)
    auth_path = os.path.join(dir_name, "auth")
    resolver_path = os.path.join(dir_name, "resolver")
    os.mkdir(dir_name)
    os.mkdir(auth_path)
    with open(os.path.join(auth_path, "root.zone"), "w") as zone_file:
        zone_file.write(".\t86400\tIN\tSOA\ta.root-servers.net.\tnstld.verisign-grs.com.\t2019072500 1800 900 604800 86400\n")
        zone_file.writelines(root_hints)

    for server, config in root_configs.items():
        server_dir = os.path.join(auth_path, server)
        os.mkdir(server_dir)
        with open(os.path.join(server_dir, "config.yaml"), "w") as f:
            yaml.dump(config, f)
    os.mkdir(resolver_path)
    with open(os.path.join(resolver_path, "root.hints"), "w") as hints_file:
        hints_file.writelines(root_hints)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Generates scenario stub based on root server delays from RIPE Atlas probes.")
    parser.add_argument("probe", type=int, action="store", help="ID of RIPE Atlas probe")
    parser.add_argument("--since", type=int, action="store", help="UNIX timestamp of minimal time of measurement. Default is 1 week in the past.", default=int(time.time())-3600*24*7)
    parser.add_argument("--until", type=int, action="store", help="UNIX timestamp of maximal time of measurement. Default is now.", default=int(time.time()))
    parser.add_argument("--no-ipv4", action="store_true", help="Don't use IPv4.")
    parser.add_argument("--no-ipv6", action="store_true", help="Don't use IPv6.")
    parser.add_argument("--no-jitter", action="store_true", help="Don't apply jitter on generated delays.")
    parser.add_argument("--no-loss", action="store_true", help="Don't calculate loss from measurements.")
    args = parser.parse_args()
    ip_versions = [4,6]
    if args.no_ipv4:
        ip_versions.remove(4)
    if args.no_ipv6:
        ip_versions.remove(6)
    generate_scenario_stub(args.since, args.until, ip_versions, args.probe, args.no_jitter, args.no_loss)
