from=selected/root_probe_little_rock_28181
for i in root_probe*
do
    cp $from/auth/root.zone $i/auth/root.zone
    cp -r $from/client $i/client
    cp $from/resolver/config.yaml $i/resolver/config.yaml
    cp $from/resolver/root.hints $i/resolver/root.hints
done