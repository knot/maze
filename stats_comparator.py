import itertools
import pytest

def worse_intervals(u, r):
    worse = []
    for i, (a, b) in enumerate(zip(u,r)):
        if a > b:
            worse.append(i)

    intervals = []
    for _, group in itertools.groupby(enumerate(worse), lambda t: t[1] - t[0]):
        group = list(group)
        intervals.append((group[0][1], group[-1][1]))

    intervals_str = " ".join(f"({i[0]/len(r):.2%}, {i[1]/len(r):.2%})" for i in intervals)

    return " Implementation under test is worse on these intervals: " + intervals_str

def test_sanity_resolver_answers_all_queries(under_test):
    assert under_test["queries"]["from_client"] == under_test["answers"]["from_resolver"]

def test_sanity_all_rcodes_are_NOERROR(under_test):
    assert under_test["rcode"] == {0: under_test["answers"]["from_resolver"]}

def test_compare_packets_sent(under_test, reference):
    # TODO: tolerance?
    u = under_test["queries"]["from_resolver"]
    r = reference["queries"]["from_resolver"]
    diff = u/r - 1
    assert u <= r, f"Implementation under test sends {diff:.0%} more packets"

def test_compare_to_nonexistent_server(under_test, reference):
    assert under_test["queries"].get("to_nonexistent_server", 0) <= reference["queries"].get("to_nonexistent_server", 0)

def test_compare_lost_packets(under_test, reference):
    u = under_test["queries"]["from_resolver"] - under_test["answers"]["from_auths"]
    r = reference["queries"]["from_resolver"] - reference["answers"]["from_auths"]
    diff = u/r - 1 if r else 1

    assert u <= r, f"Implementation under test looses {diff:.0%} more packets"

def test_histogram_comparasion(under_test, reference, hist_interval):
    u = sorted(list(under_test["rtts"].values()), reverse=True)
    r = sorted(list(reference["rtts"].values()), reverse=True)

    start = int(len(u)*hist_interval[0]/100)
    end = int(len(u)*hist_interval[1]/100)

    if all(a < b for a, b in zip(u[start:end], r[start:end])):
        return  # pass
    if all(a > b for a, b in zip(u[start:end], r[start:end])):
        pytest.fail("Implementation under test is worse on the whole interval.")
    else:
        if hist_interval == (0.0, 100.0):
            interval_string = worse_intervals(u, r)
        else:
            interval_string = ""

        pytest.skip(f"Histograms cross on this interval {hist_interval}{interval_string}.")

def test_sw_comparasion(under_test, reference, sw_interval):
    # tolerance?
    u = under_test["sliding_window"][100]
    r = reference["sliding_window"][100]

    start = int(len(u)*sw_interval[0]/100)
    end = int(len(u)*sw_interval[1]/100)

    if all(a < b for a, b in zip(u[start:end], r[start:end])):
        return  # pass
    if all(a > b for a, b in zip(u[start:end], r[start:end])):
        pytest.fail("Implementation under test is worse on the whole interval.")
    else:
        if sw_interval == (0.0, 100.0):
            interval_string = worse_intervals(u, r)
        else:
            interval_string = ""
        pytest.skip(f"Sliding window graphs cross on this interval {sw_interval}{interval_string}.")