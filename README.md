# DNS Maze

DNS Maze is a server selection test suite and testing environment for DNS resolvers. From declarative configuration of servers and network environment (delays and losses) provided in form of YAML files, it generates a set of `systemd` services with proper dependencies (e.g., resolver can only be started after all authoritative servers are started) and -- once they are run (which is done in a separated network namespace) -- provide a `pcap` file of all the traffic between all parts of the scenario (client, resolver and authoritatives).

_Some findings made using DNS Maze were presented at [DNS OARC 31](https://indico.dns-oarc.net/event/32/contributions/711/) in October 2019. Both [slides](https://indico.dns-oarc.net/event/32/contributions/711/attachments/705/1200/resolver_server_selection.pdf) and [video](https://youtu.be/z7Jl1sjr9jM?list=PLCAxS3rufJ1ebDBMr7tfiD368_VyfzSju&t=459) are available_.

## Requirements and install

### Requirements

Needed:
* `python3` (used to generate _everything_, at least version 3.6)
* `pyyaml` (YAML parser for Python)
* `python-jinja` (templating)
* Knot DNS (authoritative server of _our_ choice, version 2.9.0 was used while developing this)
* _recent_ version of `systemd` (used as process manager)
* Wireshark (`tshark` is used for capturing the traffic)
* `dnsperf` (version 2.3.2 was used during development)

Resolvers supported _out-of-the-box_:

_See [this](#resolver-resolver-directory)_

PCAP analysis:
* `matplotlib`
* `numpy`
* `dpkt`

Optional (but used in some scenarios)
* `libfaketime` (used for faking time for the resolver -- useful in DNSSEC scenarios)

### Install

Install all requirements, clone this repository, and try to [run it](#running-dns-maze).

## Running DNS Maze

### First run

First, we need to copy the environment to a temporary folder and generate all the services:

```bash
$ ./process_generator.py scenarios/selected/simple
sudo ./reload_services /tmp/maze-simple-tmy884_f maze maze-dnsperf
```

_Note: Knot Resolver is used for this scenario, so either install it or [edit the scenario](#resolver-resolver-directory) to use your favorite resolver._

Output of `process_generator.py` is a command used to clean-up services from the previous run, enable all services needed and run the client service, which in turn will run _all_ the services. Note that the path of the temporary folder will be different for each run.

Before running the outputted command, one can inspect the generated environment by navigating to the `/tmp/maze-simple-xyz` folder.

Once `sudo ./reload_services /tmp/maze-simple-tmy884_f maze maze-dnsperf` is run, a lot of symlinks are created, and `maze-dnsperf` is started in the background.

_Note: Root privileges are necessary since `systemd` does not support using network namespaces (`PrivateNetwork=` option) when running services as unprivileged user. See [`systemd` issue 14005](https://github.com/systemd/systemd/issues/14005)._

If everything went according to the plan, one could use `systemd status "maze-*"` to see the status of all services generated and run by DNS Maze.

After a while (depending on the complexity of the scenario) a PCAP file `result_kresd.pcap` will appear in the scenario's folder.

### `process_generator.py` command line options

```
$ ./process_generator.py --help
usage: process_generator.py [-h] [--prefix PREFIX]
                            [--service-templates SERVICE_TEMPLATES]
                            [--config-templates CONFIG_TEMPLATES]
                            [--contrib CONTRIB] [--pcap PCAP]
                            [--faketime FAKETIME]
                            scenario

Generates systemd services. Outputs a command to run them.

positional arguments:
  scenario              Path to scenario directory.

optional arguments:
  -h, --help            show this help message and exit
  --prefix PREFIX       Prefix for systemd unit for easier clean-up. (default: maze)
  --service-templates SERVICE_TEMPLATES
                        Path to directory with service templates. (default: $PWD/systemd)
  --config-templates CONFIG_TEMPLATES
                        Path to directory with config file templates. (default: $PWD/configs)
  --contrib CONTRIB     Path to directory with external tools. (default: $PWD/contrib)
  --pcap PCAP           Path to write a temporary pcap file. (default: /tmp/dump.pcap)
  --faketime FAKETIME   Path to libfaketime shared object file. (default: /usr/lib/faketime/libfaketime.so.1)
```

For regular usage, `scenario` argument should be only one needed.

`--prefix` is useful for generating multiple independent scenarios and running them afterward.

Path of `faketime` differs on various distribution, so if it is present in the selected scenario, you might have to specify it.

## Writing scenarios and generating services

### Scenario structure

Scenario is defined by a directory (default path for these is `maze/scenarios/`) and contains a declarative definition of all processes needed.

There are three subdirectories with different purposes to describe the whole environment to be simulated: **auth**, **resolver**, **client**.

In the following example, I'll use the `simple` scenario found on [here](./scenarios/selected/simple), so it might be beneficial to look at its corresponding directory in order to better understand what is going on.

#### Authoritative servers (`auth` directory)

_Example taken from `maze/scenarios/selected/simple/auth/root/a/config.yaml`:_

```yaml
name: "a-root-server"
service: "knot"
config: "knotd.conf"
address: ["1.0.0.10"]
netem: "delay 10ms"
domain: "."
zonefile: "../root.zone"
```

Using `config.yaml` files in an `auth` subdirectory structure, one can orchestrate an arbitrary number of authoritative servers (we are using Knot DNS instances) with arbitrary zone data.

##### Options

|Option     | Explanantion|
|---        |---|
|`name`     | Name of generated systemd service (with `prefix`, see [command line options](#process_generator.py-command-line-options) above)|
|`service`  | Jinja template name present in `maze/systemd`. `knot` therefore points to `maze/systemd/knot.service.j2`. Selected template is used to generate the service file.|
|`config`   | Jinja template name present in `maze/configs`. `knotd.conf` therefore points to `maze/systemd/knotd.conf`. Selected template is used to generate the configuration file.|
|`address`  | List of IP addresses for the server to listen on.|
|`netem`    | Network emulator options. See [this](https://wiki.linuxfoundation.org/networking/netem) for more information. Used to specify loss and delay per server.|
|`domain`   | Domain of the zone|
|`zonefile` | Path to a zone file relative to `config.yaml`. Note that it can not be outside of the directory structure.|

#### Resolver (`resolver` directory)

_Example taken from `maze/scenarios/selected/simple/resolver/config.yaml`:_

```yaml
name: "resolver"
service: "kresd"
address: ["1.0.0.1"]
roothints: "root.hints"
config: "kresd.conf"
```

Most of the options are the same as above. `roothints` are is a relative path to root hint file for resolver. Configuration and service templates for these resolver implementations is provided in `maze/configs` and `maze/systemd` respectively:

| Resolver          | `config`          | `service` |
| ---               | ---               | ---       |
| Knot Resolver     | `kresd.conf`      | `kresd`   |
| PowerDNS Recursor | `recursor.conf`   | `recursor`|
| Unbound           | `unbound.conf`    | `unbound` |
| Bind              | `named.conf`      | `named`   |

Changing the `resolver/config.yaml` file to

```yaml
name: "resolver"
service: "unbound"
address: ["1.0.0.1"]
roothints: "root.hints"
config: "unbound.conf"
```

will change the resolver under test to Unbound.

#### Client (`client` directory)

_Example taken from `maze/scenarios/selected/simple/client/config.yaml`:_

```yaml
name: "dnsperf"
service: "dnsperf"
address: ["2.0.0.1"]
dnsperf: -d q
```

`dnsperf` is used to send queries to the resolver under test. The `dnsperf` option is used to pass arguments to it (in this case, it will take the queries from a file called `q` present in the `simple/client` directory).

It is possible to use some other program as a client. Service file template for it must be provided in the `systemd` directory.

### Miscealanious options

| Option        | Explanation
| ---           | ---
| `dnssec`      | Turn on/off DNSSEC
| `faketime`    | Process will be started with `FAKETIME` environment variable set to the value of this option. See [this](https://github.com/wolfcw/libfaketime) for details.
| `starttime`   | Time of start of this process in seconds. (`starttime:10` will cause 10 s delay between start of simulation and start of this server).
| `stoptime`    | Time of stop of this process in seconds. (Timing options are available only for authoritatives.)

## Advanced usage and details

It is possible to write your own service and config file template to support other software. This should not be a problem with basic knowledge of `systemd` unit files and configuration file structure of one's software.

The same applies to adding more configurable features to existing resolver configuration templates.

Some notes to this:

* Use `{% include 'common.service.j2' %}` at the start of a service file template, this will ensure that dependencies and network settings of the process are properly set up.
* If you choose to use different authoritative server than Knot DNS (_although I can't see a reason for that…_), pay close attention to `Before=`, `After=`, `WantedBy=` and `RuntimeMaxSec=` options specified in `systemd/knot.service.j2` to ensure proper functionality. You will also need to provide a new configuration template in `configs/`.
* `{prefix}-{name_of_service}-net.service` is generated for each service with `address` option set. It takes care of handling addresses on an interface as well as setting up `netem`.
* `{prefix}-{name_of_service}-timer` is generated for each service with `starttime` option set. It ensures the delay of start of the server.
* `{prefix}-namespace.service` is used to set up the network and packet capture.


Service dependencies ensure that this order of execution is followed:

1. Network namespace and interfaces are set up, and packet capture is started (using `{prefix}-namespace.service`).
2. Each authoritative server is started (with corresponding `*-net.service`).
3. Resolver is started (with corresponding `*-net.service`).
4. Client is started (with corresponding `*-net.service`).
5. Client finishes execution.
6. Resolver is stopped.
7. Authoritatives are stopped.
8. Network capture is stopped, PCAP is copied to original scenario directory, network namespace is deleted.

## Other tools

`ripe_generator.py` can be used to generate stubs of scenarios using data from RIPE Atlas probes. See `./ripe_generator.py --help` for details.

`pcap_analyzer.py` is a messy and poorly documented prototype of a tool for visualizing and comparing results from multiple resolvers.

## License

DNS Maze is distributed under [GNU GPL v3](./LICENSE.md).
