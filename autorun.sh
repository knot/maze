#!/bin/bash
# This script automates whole Maze's machinery.
# It accepts one command line argument, the scenario and puts results in the appropriate places.
set -x
SCENARIO=$1
HIST_INTERVALS="0,100"
SW_INTERVALS="0,100"
pushd $(dirname $0) > /dev/null
RELOAD_COMMAND=$(./process_generator.py $SCENARIO)
PCAP=$($RELOAD_COMMAND)
sleep 5
PICKLED=$(./pcap_analyzer.py --pcap $PCAP --scenario $SCENARIO)

python3 -m pytest stats_comparator.py -k "sanity" -v -rs --under-test $PICKLED

if [ $? -ne 0 ]; then
    echo "Sanity check failed, something is seriously broken."
    exit 1
fi

GRAPH_ARGS="--resolver $PICKLED"

mkdir -p $SCENARIO/reports
for ref in $(ls $SCENARIO/references/*.pickle)
do
    GRAPH_ARGS="$GRAPH_ARGS --resolver $ref"
    tmp=$(basename $ref)
    NAME=${tmp%%*/}
    python3 -m pytest stats_comparator.py -k "not sanity" -v -rs --junit-xml $SCENARIO/reports/$NAME.xml --under-test $PICKLED --reference $ref --hist-intervals $HIST_INTERVALS --sw-intervals $SW_INTERVALS
done

cp $PICKLED $SCENARIO/references/result_previous_stats.pickle

./graph_plotter.py $GRAPH_ARGS --scenario $SCENARIO
